/* To compile:
g++ -std=c++14 -Wall -Wno-sign-compare -fsanitize=address -fno-omit-frame-pointer -ggdb
*/
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
// -------------------------------------------------------------------------------
class Separator {
  public:
    Separator(const string &value) : m_value(value), m_fresh(true) {}
    Separator(string &&value) : m_value(std::move(value)), m_fresh(true) {}

    const string &get() {
      if (m_fresh) {
        m_fresh = false;
        return m_emptyStr;
      }
      return m_value;
    }
  private:
    bool m_fresh;
    std::string m_value;
    const static std::string m_emptyStr;
};

const std::string Separator::m_emptyStr;
// -------------------------------------------------------------------------------

int main() {
	vector<int> vec({ 3, 8, 1, 3, 11, 4 });
  sort(vec.begin(), vec.end());

  Separator sep(", ");
  for (auto vv : vec) {
    cout << sep.get() << vv;
  }
  cout << endl;
	return 0;
}
