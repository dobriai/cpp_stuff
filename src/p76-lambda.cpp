#include <iostream>
#include <functional>
#include <list>
#include <algorithm>

class Widget
{
 public:
   void doSomething();

 private:
   std::list<int> m_li;
   int m_minVal;
};

void Widget::doSomething()
{
   auto it = std::find_if(cbegin(m_li), cend(m_li),
                          // Errors:
                          // [m_minVal](int i) { return i > m_minVal; }
                          // [        ](int i) { return i > m_minVal; }
                          [this](int i) { return i > m_minVal; }
   );
}

int main()
{
   std::cout << "Done!" << std::endl;
}
