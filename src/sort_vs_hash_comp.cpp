// Build with:
//    g++ -Wall -O3 ...
// And experiment with -O<n>, maybe.
// May have to use the some g++ optinon, like: -std=c++1z
//
// Run:
//    ./a.out 2 10000
// which will generate a vector of 10k ints. The hash size will be 2*10k.
//
// Under Microsoft Windows, open "x64 Native Command Prompt for VS20xx" and:
//    cl /EHsc /std:c++17 /O2 /D RUN_NxN_TEST sort_vs_hash_comp.cpp
//
// SUMMARY:
// Compares two vector<int> sequences: sort vs unordered_set.
// If the macro RUN_NxN_TEST is defined, also does a NxN linear search.
//
// Suppose you have to compare two vectors of things, e.g. int-s.
// Which is faster - sort + binary_search OR build a hash?
//
// Yeah, there are many fine details regarding the distrubution,
// what is const/mutable, etc.
//
// This is just a quick and dirty experiment...
//
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <random>
#include <stdexcept>
#include <unordered_set>

using namespace std;

const int num_rounds = 5;
// #define RUN_NxN_TEST  // Run/skip the slow N^2 test

class TimeAccumulator
{
public:
   TimeAccumulator() {}

   void add(string key, unsigned time_spent)
   {
      auto &entry = m_key2timeNcount[key];
      entry.first += time_spent;
      ++entry.second;
   }

   auto adder(string key)
   {
      return [this, key](unsigned time_spent) {
         add(key, time_spent);
      };
   }

   void dump()
   {
      for (const auto &item : m_key2timeNcount)
         cout << setw(10)
              << item.second.first / item.second.second
              << " us average time for " << item.first
              << endl;
   }

private:
   map<string, pair<unsigned, unsigned>> m_key2timeNcount;
};

auto time_it(std::function<void()> func, std::function<void(unsigned)> timer)
{
   auto t1 = std::chrono::high_resolution_clock::now();
   func();
   auto t2 = std::chrono::high_resolution_clock::now();

   auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

   if (timer)
      timer(duration);

   return duration;
}

vector<int> make_random_vec(size_t vec_size)
{
   // random_device rd;
   // cout << "Random unsigned int = " << rd() << endl;
   static const unsigned int rand_seed = 2205094566;
   //
   mt19937 gen(rand_seed); //Standard mersenne_twister_engine seeded with rd()
   uniform_int_distribution<> dis(-1, numeric_limits<int>::max());

   vector<int> numbers;
   numbers.reserve(vec_size);

   for (size_t ii = 0; ii < vec_size; ++ii)
      numbers.push_back(dis(gen));

   return numbers;
}

void throw_unless(bool cond, const char *pMsg)
{
   if (!cond)
      throw invalid_argument(pMsg);
}

// Simulating work to prevent optimizations that are so smart that they just do nothing at all!
//
// int s_uselessCount = 0;
// void __attribute__((noinline)) uselessWork(bool uselessBool)
// {
//    if (uselessBool)
//       s_uselessCount += 1;
//    else
//       s_uselessCount -= 1;
// }

int main(int argc, char *argv[])
{
   throw_unless(argc == 3, "Must specify: <hash_factor> <vec_size>");

   const unsigned int hash_factor = atoi(argv[1]);
   const size_t vec_size = atoi(argv[2]);

   throw_unless(0 < hash_factor && hash_factor < 16, "Hash factor must be [1..15]");
   throw_unless(0 < vec_size, "vec_size must be greater than zero");

   vector<int> numbers(make_random_vec(vec_size));

   auto sortInts = [&numbers]() {
      vector<int> sorted(numbers);
      sort(sorted.begin(), sorted.end());
      for (int ii = numbers.size() - 1; ii >= 0; --ii)
         if (!binary_search(sorted.begin(), sorted.end(), numbers[ii]))
            return;
      ;
   };

   auto hashInts = [&numbers, hash_factor]() {
      unordered_set<int> hash(numbers.begin(), numbers.end(), hash_factor * numbers.size());
      for (int ii = numbers.size() - 1; ii >= 0; --ii)
         if (!hash.count(numbers[ii]))
            return;
   };

   [[maybe_unused]] auto plainNxN = [&numbers]() {
      vector<int> sameNumbers(numbers);
      for (int ii = sameNumbers.size() - 1; ii >= 0; --ii)
         if (std::find(numbers.begin(), numbers.end(), sameNumbers[ii]) == numbers.end())
            return;
   };

   TimeAccumulator acc;

   for (int ii = 0; ii < num_rounds; ++ii)
   {
      cout << "sortInts() = " << time_it(sortInts, acc.adder("sort")) << endl;
      cout << "hashInts() = " << time_it(hashInts, acc.adder("hash")) << endl;
#ifdef RUN_NxN_TEST
      cout << "plainNxN() = " << time_it(plainNxN, acc.adder("plain")) << endl;
#endif
   }

   cout << endl;
   acc.dump();
}
