// Use the -std=c++1z option!
//
// #include <functional>
#include <iostream>
#include <regex>
#include <string>

// using namespace std;

int main()
{
   // const std::wstring fnames[] = {L"foo.txt", L"bar.txt", L"baz.dat", L"zoidberg"};
   // const std::wregex txt_regex(L"[a-z]+\\.txt");
   const std::wstring fnames[] = {
      L"FMCEE::Content cluster of Family id = 110386 (Family): Brick Standard with EpisodeId = 846 needs restamping to 848",
      L"FMCEE::Loaded Family had an Episode older than its Content Docs: id = 110386 (Family): Brick Standard"
      };
   const std::wregex txt_regex(L"FMCEE::Loaded Family had an Episode older than its Content Docs: id = .*",
                               std::regex_constants::ECMAScript);

   for (const auto &fname : fnames)
   {
      std::wcout << fname << ": " << std::regex_match(fname, txt_regex) << '\n';
   }
   std::cout << std::endl;
}
