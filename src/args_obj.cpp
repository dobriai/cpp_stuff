// Use the -std=c++1z option!
//
#include <iostream>

#define PAccess(MEMBER) \
   auto set##MEMBER(decltype(m_p##MEMBER) pSth) { m_p##MEMBER = pSth; return *this; } \
   auto get##MEMBER() const { return m_p##MEMBER; }
#define VAccess(MEMBER) \
   auto set##MEMBER(const decltype(m_v##MEMBER) &sth) { m_v##MEMBER = sth; return *this; } \
   auto set##MEMBER(decltype(m_v##MEMBER) &&sth) { m_v##MEMBER = std::move(sth); return *this; } \
   auto get##MEMBER() const { return m_v##MEMBER; }

#define PMember(TYPE, MEMBER) \
   private: \
      TYPE *m_p##MEMBER = nullptr; \
   public: \
      PAccess(MEMBER)
#define VMember(TYPE, MEMBER, VALUE) \
   private: \
      TYPE m_v##MEMBER = VALUE; \
   public: \
      VAccess(MEMBER)

struct Opts {
   int m_vFactor = 3;
   VAccess(Factor);
};

struct ArgsOut {
   int *m_pRank = nullptr;
   int *m_pCount = nullptr;
   PAccess(Rank);
   PAccess(Count);
};

void func(const Opts &opts, const ArgsOut &outs) {
   if (outs.m_pRank)
      *outs.m_pRank += opts.m_vFactor;
   if (outs.m_pCount)
      *outs.m_pCount += 2*opts.m_vFactor;
}

class SomeClass {
 public:
   struct ComputeStuffX {
      ComputeStuffX(int width, int length) : m_vWidth(width), m_vLength(length) {}
      VMember(int, Width, 1);
      VMember(int, Length, 1);
      VMember(int, Height, 1);
      PMember(int, BaseArea);
      PMember(int, FullArea);
      PMember(int, Volume);
   };
   void computeStuffX(const ComputeStuffX &args) const;
   void operator()(const ComputeStuffX &args) const;

   struct ComputeStuffY {
      ComputeStuffY(const SomeClass *pThis, int width, int length) : m_pThis(pThis), m_vWidth(width), m_vLength(length) {}
      void run() const;
      const SomeClass *m_pThis;
      VMember(int, Width, 1);
      VMember(int, Length, 1);
      VMember(int, Height, 1);
      PMember(int, BaseArea);
      PMember(int, FullArea);
      VMember(int *, Volume, nullptr); // This also works
   };
   ComputeStuffY computeStuffY(int width, int length) const { return ComputeStuffY(this, width, length); }
 private:
   int m_scale = 2;
};

void SomeClass::computeStuffX(const ComputeStuffX &args) const {
   const int areaScale = m_scale * m_scale;
   if (args.getBaseArea())
      *args.getBaseArea() = args.getWidth() * args.getLength() * areaScale;
   if (args.getFullArea())
      *args.getFullArea() = (args.getWidth() * args.getLength() 
      + (args.getWidth() + args.getLength()) * args.getHeight()) * 2 * areaScale;
   if (args.getVolume())
      *args.getVolume() = args.getWidth() * args.getLength() * args.getHeight() * areaScale * m_scale;
}

void SomeClass::operator()(const ComputeStuffX &args) const {
   computeStuffX(args);
}

void SomeClass::ComputeStuffY::run() const {
   const int areaScale = m_pThis->m_scale * m_pThis->m_scale;
   if (getBaseArea())
      *getBaseArea() = getWidth() * getLength() * areaScale;
   if (getFullArea())
      *getFullArea() = (getWidth() * getLength() 
      + (getWidth() + getLength()) * getHeight()) * 2 * areaScale;
   if (getVolume())
      *getVolume() = getWidth() * getLength() * getHeight() * areaScale * m_pThis->m_scale;
}

int main() {
   int aa = 0, bb = 0;
   func(Opts(), ArgsOut());
   std::cout << "aa = " << aa << ", bb = " << bb << "\n";
   func(Opts(), ArgsOut().setRank(&aa));
   std::cout << "aa = " << aa << ", bb = " << bb << "\n";
   func(Opts().setFactor(13), ArgsOut().setRank(&aa).setCount(&bb));
   std::cout << "aa = " << aa << ", bb = " << bb << "\n";

   SomeClass sc;
   int vol = -1;
   sc.computeStuffX(SomeClass::ComputeStuffX(3, 5).setVolume(&vol));
   std::cout << "vol = " << vol << "\n";

   vol = -1;
   sc(SomeClass::ComputeStuffX(3, 5).setVolume(&vol));
   std::cout << "vol = " << vol << "\n";

   vol = -1;
   sc.computeStuffY(3, 5).setVolume(&vol).run();
   std::cout << "vol = " << vol << "\n";
}
