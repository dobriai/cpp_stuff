// -std=c19 or =std=2x
#include <functional>
#include <optional>
#include <iostream>

template <typename FT, typename PT = std::result_of_t<FT()>>
class call_once {
 public:
   explicit call_once(FT func) noexcept : m_func(std::move(func)) {}
   call_once(call_once &&other) noexcept
       : m_func(std::move(other.m_func)), m_res(std::move(other.m_res)) {}

   call_once(const call_once &) = delete;
   call_once &operator=(const call_once &) = delete;
   call_once &operator=(call_once &&) = delete;

   PT operator()() {
      if (!m_res.has_value())
         m_res = m_func();
      return m_res.value();
   }

 private:
   FT m_func;
   std::optional<PT> m_res;
};

int main() {
   int ii = 17;
   call_once xx([&ii]() { ++ii; return &ii; });

   std::cout << "ii = " << ii << "\n";
   std::cout << "xx = " << *xx() << "\n";
   std::cout << "xx = " << *xx() << "\n";
}
