/* To compile:
g++ -std=c++14 -Wall -fsanitize=address -fno-omit-frame-pointer -ggdb
*/
#include <iostream>
#include <sstream>
#include <vector>
// #include <algorithm>
#include <memory>
using namespace std;

// -------------------------------------------------------------------------------
using NodeId = int;
const NodeId invalidNodeId = -1;

template<typename VT> class NodeTable;

template <typename VT>
class Node2
{
public:
  Node2(VT value) : m_value(value) {}

  const VT &getValue() { return m_value; }
  Node2<VT> setValue(const VT &value) { m_value = value; return *this; }
  Node2<VT> setValue(VT &&value) { m_value = move(value); return *this; }

  NodeId getId() const { return m_id; }
  NodeId getChildId(int which) const { return which == 0 ? m_leftId : m_rightId; }

  Node2<VT> &setChildId(int which, NodeId nodeId)
  {
    *(which == 0 ? &m_leftId : &m_rightId) = nodeId;
    return *this;
  }

  string getDbgStr() const
  {
    ostringstream str;
    str << m_value << '(' << m_id << ')';
    return str.str();
  }

private:
  friend class NodeTable<VT>;
  VT m_value;
  NodeId m_id = invalidNodeId;
  NodeId m_leftId = invalidNodeId, m_rightId = invalidNodeId;
};

template <typename VT>
class NodeTable
{
public:
  using NodeT = Node2<VT>;
  void aquire(unique_ptr<NodeT> &&oNode)
  {
    if (!oNode.get())
      return;
    oNode.get()->m_id = m_nodes.size();
    m_nodes.push_back(move(oNode));
  }
  void remove(NodeId nodeId)
  {
    if (validId(nodeId))
      m_nodes[nodeId].reset(nullptr);
  }
  const NodeT *get(NodeId nodeId) const
  {
    return validId(nodeId) ? m_nodes[nodeId].get() : nullptr;
  }
  NodeT *getFW(NodeId nodeId) const
  {
    return validId(nodeId) ? m_nodes[nodeId].get() : nullptr;
  }

  NodeId getNextId() const { return m_nodes.size(); }

private:
  bool validId(NodeId nodeId) const { return 0 <= nodeId && size_t(nodeId) < m_nodes.size(); }
private:
  vector<unique_ptr<NodeT>> m_nodes;
};
// -------------------------------------------------------------------------------

using TableT = NodeTable<char>;
using NodeT = TableT::NodeT;

int main()
{
  TableT doc;
  for (int ii = 0; ii < 12; ++ii)
    doc.aquire(make_unique<NodeT>('A'+ii));

  doc.getFW(0)->setChildId(0, 1).setChildId(1, 2);
  doc.getFW(1)->setChildId(0, 3).setChildId(1, 4);
  doc.getFW(2)->setChildId(0, 5).setChildId(1, 6);
  doc.getFW(3)->setChildId(0, 7).setChildId(1, 8);
  doc.getFW(7)->setChildId(0, 9);
  doc.getFW(9)->setChildId(0, 10);
  doc.getFW(4)->setChildId(1, 11);

  static int s_nullIdx = 0; // For printing
  cout << "Paste the output here: http://www.webgraphviz.com" << endl << endl;
  cout << "digraph G {" << endl;
  for (int ii = 0, iiMax = doc.getNextId(); ii < iiMax; ++ii)
  {
    if (const NodeT *pNode = doc.get(ii))
    {
      for (int which = 0; which < 2; ++which)
      {
        string kidStr;
        if (const NodeT *pKid = doc.get(pNode->getChildId(which)))
          kidStr = pKid->getDbgStr();
        else
        {
          kidStr = static_cast<ostringstream &>(ostringstream() << "_xxx_" << ++s_nullIdx).str();
          cout << "   " << kidStr  << " [shape=point];" << endl;
        }
        cout << "   \"" << pNode->getDbgStr() << "\" -> \"" << kidStr << '"' << endl;
      }
    }
  }
  cout << "}" << endl;

  // new NodeT('L'); // Test leak detection
  return 0;
}
