/* To compile:
g++ -std=c++14 -Wall -fsanitize=address -fno-omit-frame-pointer -ggdb
*/
#include <iostream>
#include <sstream>
#include <vector>
// #include <algorithm>
#include <memory>
using namespace std;

struct Aaa
{
    int m_ii;
    char m_ch;
};

// Test structured binding - reference vs not a ref
//
// Expected output:
// auto [ii, ch]:
// ii = 19, aa.m_ii = 17
// auto &[ii, ch]:
// ii = 19, bb.m_ii = 19
//
int main()
{
    // Modify a binding to a temp object
    cout << "auto [ii, ch]:\n";
    {
        Aaa aa{17, 's'};
        auto [ii, ch] = aa;
        ii = 19;
        cout << "ii = " << ii << ", aa.m_ii = " << aa.m_ii << endl;
    }
    // Modify a binding to the real object
    cout << "auto &[ii, ch]:\n";
    {
        Aaa bb{17, 's'};
        auto &[ii, ch] = bb;
        ii = 19;
        cout << "ii = " << ii << ", bb.m_ii = " << bb.m_ii << endl;
    }
}
