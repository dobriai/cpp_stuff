#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <map>
#include <numeric>

using namespace std;

template <typename IN, typename OUT = vector<typename IN::mapped_type>>
OUT values(const IN &map) {
   OUT out;
   for (const auto &[kk, vv]: map)
      out.emplace_back(vv);
   return out;
}

int main()
{
   map<int, string> i2sMap{{1, "one"}, {7, "seven"}, {8, "eight"}};

   cout << "Some map<int, string>:" << endl;
   for (const auto &i2s : i2sMap)
      std::cout << i2s.first << " --> " << i2s.second << std::endl;

   // accumulate (similar to reduce, apparently)
   {
      const vector<string> vals =
          accumulate(i2sMap.cbegin(), i2sMap.cend(), vector<string>(),
                     [](auto &vec, const auto &i2s) { vec.push_back(i2s.second); return vec; });

      cout << "Map values via accumulate():" << endl;
      for (const auto &vv : vals)
         cout << vv << endl;
   }

   // transform
   {
      vector<string> vals;
      transform(i2sMap.cbegin(), i2sMap.cend(), back_inserter(vals),
                [](const auto &i2s) { return i2s.second; });

      cout << "Map values via transform():" << endl;
      for (const auto &vv : vals)
         cout << vv << endl;
   }

   // by hand
   {
      const vector<string> vals = ([&i2sMap]() {
         vector<string> vals;
         for (const auto &[kk, vv] : i2sMap)
            vals.emplace_back(vv);
         return vals;
      }());

      cout << "Map values by hand:" << endl;
      for (const auto &vv : vals)
         cout << vv << endl;
   }

   // using a utility
   {
      cout << "Map values via values():" << endl;
      for (const auto &vv : values(i2sMap))
         cout << vv << endl;
   }

}
