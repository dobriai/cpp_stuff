#include <functional>
#include <iostream>

using namespace std;

int a {-2};

function<int(int)> returnClosure()
{
   static int b = 3, c = 7;

   return [](int x) { return a * x * x + b * x + c; };
}

int main()
{
   auto f = returnClosure();
   cout << f(3) << endl;
}
