#include <bits/stdc++.h>

struct Aaa {
   int m_int = -1;
};

static int g_count = 0;

template<const Aaa &aa>
std::string func() {
   static const int s_cc = ++g_count;
   return (std::stringstream() << "[" << aa.m_int << ", " << s_cc << "]").str();
}

static int g_count2 = 1000;

template<Aaa &aa>
std::string funcNC() {
   static const int s_cc = ++g_count2;
   return (std::stringstream() << "NC: [" << aa.m_int << ", " << s_cc << "]").str();
}

constexpr Aaa aa3{3};
constexpr Aaa aa7{7};
Aaa aa13{13};

void incAaa(Aaa *pAaa)
{
   ++pAaa->m_int;
}

int main() {
   std::cout << "Hi\n";
   std::cout << func<aa3>() << "\n";
   std::cout << func<aa7>() << "\n";
   std::cout << func<aa3>() << "\n";
   std::cout << func<aa7>() << "\n";
   std::cout << funcNC<aa13>() << "\n";
   incAaa(&aa13);
   std::cout << funcNC<aa13>() << "\n";
}
