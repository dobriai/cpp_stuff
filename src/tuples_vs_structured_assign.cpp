// Use the -std=c++1z option!
//
// #include <functional>
#include <iostream>
#include <tuple>

using namespace std;

int main()
{
   auto [aa, bb] = make_tuple(12, 2.718);
   cout << "aa = " << aa << ", bb = " << bb << endl;

   int xx;
   tie(xx, ignore) = make_tuple(17, 2.718);
   cout << "xx = " << xx << endl;
}
