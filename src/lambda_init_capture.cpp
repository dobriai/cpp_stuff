// Use the -std=c++1z option!
//
// #include <functional>
#include <iostream>
#include <vector>

using namespace std;

auto getLam()
{
    return [vec = vector<int>({1, 2, 3}), count = 0]() mutable {
        cout << "count, size = " << count++ << ", " << vec.size() << endl;
        vec.push_back(count + 7);
    };
}

int main()
{
    [xx = int(77)]() {
        cout << "xx = " << xx << endl;
    }();

    auto lam = getLam();
    lam();
    lam();
    lam();
}
