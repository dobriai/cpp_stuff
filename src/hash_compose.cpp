// Use the -std=c++1z option!
//
// #include <functional>
#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Xxx
{
public:
   Xxx(int id) : m_id(id) {}
   int asInteger() const { return m_id; }
   bool operator ==(Xxx other) const { return m_id == other.m_id; }

private:
   int m_id = -1;
};

template<> struct std::hash<Xxx>
{
   size_t operator()(Xxx xxx) const
   {
      return hash<int>()(xxx.asInteger());
   }
};

int main()
{
   vector<Xxx> xxx = {3, 13, 23, 44, 3, 13};
   cout << "xxx = " << xxx.size() << endl;

   unordered_set<Xxx> sxx(12);
   cout << "bucket_count = " << sxx.bucket_count() << endl;
   sxx = {3, 13, 23, 44, 3, 13};
   cout << "bucket_count = " << sxx.bucket_count() << endl;
   for (auto key : sxx)
      cout << key.asInteger() << endl;
}
