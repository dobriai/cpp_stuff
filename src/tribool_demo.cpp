// On older gcc compilers, maybe use the -std=c++1z option!
#include <iostream>
#include <boost/logic/tribool.hpp>
//
// W/o this niclude, ostream <<() prints "false" instead of "intermediate" - why?!
#include <boost/logic/tribool_io.hpp>

using namespace std;
// using namespace boost::logic;

int main()
{
   const boost::logic::tribool tb = boost::logic::indeterminate;

#define PRINT_RESULT(test) \
   cout << #test " => " << (test) << endl

   cout << boolalpha;
   PRINT_RESULT(boost::logic::get_default_indeterminate_name<ostream::char_type>());
   PRINT_RESULT(boost::logic::indeterminate(tb));
   PRINT_RESULT(tb);
   PRINT_RESULT(!tb);
   PRINT_RESULT(tb == true);
   PRINT_RESULT(tb != true);
   PRINT_RESULT(tb == false);
   PRINT_RESULT(tb != false);
   PRINT_RESULT(tb == boost::logic::indeterminate);
   PRINT_RESULT(tb != boost::logic::indeterminate);
   PRINT_RESULT(tb == tb);
   PRINT_RESULT(tb != tb);
   PRINT_RESULT(bool(tb));
   PRINT_RESULT(bool(!tb));
   PRINT_RESULT(!bool(tb));
   PRINT_RESULT(!bool(!tb));
   cout << "--- Bahhh! ---\n";
}

/* Expected output:

boost::logic::get_default_indeterminate_name<ostream::char_type>() => indeterminate
boost::logic::indeterminate(tb) => true
tb => indeterminate
!tb => indeterminate
tb == true => indeterminate
tb != true => indeterminate
tb == false => indeterminate
tb != false => indeterminate
tb == boost::logic::indeterminate => indeterminate
tb != boost::logic::indeterminate => indeterminate
tb == tb => indeterminate
tb != tb => indeterminate
bool(tb) => false
bool(!tb) => false
!bool(tb) => true
!bool(!tb) => true
--- Bahhh! ---

*/